import Layout from  '../components/Layout' ;
import Link from 'next/link' ;

const PostLink = ({ slug, title }) => (
    <li>
     <Link as={`/${slug}`} href={`/post?title=${title}`}>
        <a>{title}</a>
    </Link>
</li>
)

export default  () => (
    <Layout title="My Blog">
        <ul>
            <PostLink  slug="react-post"title="React" />
            <PostLink  slug="angular-post"title="Angular" />
            <PostLink  slug="HTML-post" title="HTML" />
            <PostLink  slug="CSS-post" title="CSS" />
            <PostLink  slug="WP-post" title="Wordpress" />
        </ul>
    </Layout>
)