import Layout from '../components/Layout';
import { withRouter } from 'next/router';

const Post = ({ router }) => (
<Layout title={router.query.title}>

<p style={{ width :  "80vw"}}> Nihil est enim virtute amabilius, nihil quod magis adliciat ad diligendum, 
quippe cum propter virtutem et probitatem etiam eos, quos numquam vidimus, 
quodam modo diligamus. Quis est qui C. Fabrici, M'. Curi non cum caritate aliqua 
benevola memoriam usurpet, quos numquam viderit? quis autem est, qui Tarquinium 
Superbum, qui Sp. Cassium, Sp. Maelium non oderit? Cum duobus ducibus de imperio 
in Italia est decertatum, Pyrrho et Hannibale; ab altero propter probitatem eius non nimis 
alienos animos habemus, alterum propter crudelitatem semper haec civitas oderit. </p>
</Layout>
);

export default withRouter (Post);