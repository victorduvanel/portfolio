webpackHotUpdate("static/development/pages/about.js",{

/***/ "./pages/_error.js":
/*!*************************!*\
  !*** ./pages/_error.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
var _jsxFileName = "/Users/victorduvanel/DEV/REACT JS/next-portfolio/pages/_error.js";


/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var statusCode = _ref.statusCode;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
    title: "error!!!",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    __self: this
  }, statusCode ? "Could not load your user data: Status Code ".concat(statusCode) : "Couldn't get that page, sorry !");
});

/***/ })

})
//# sourceMappingURL=about.js.47f679323133181df138.hot-update.js.map